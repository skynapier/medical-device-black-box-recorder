#include "data_util.h"
#include "write_file.cpp"

/**
 * implementation of covert string to float, int, double
 */
template <class Type>  
Type stringToNum(const string& str)  
{  
    istringstream iss(str);  
    Type num;  
    iss >> num;  
    return num;      
}  

/**
 * read csv file with 3 columns have different variables
 * implementation of split method from sstream
 */ 
vector<DataUtil* > ReadFile(string file_dir)
{
	
    vector<DataUtil*> ret;
    vector<string> meta_data;
    vector< vector<float> > data; 
 
	ifstream inFile(file_dir, ios::in); 
	string lineStr;

    int count = 0;
	while(getline(inFile,lineStr)) //getline from sstream
	{
		//cout << lineStr <<endl;
		stringstream ss(lineStr);

		string value;
        if( count == 0)
        {
            while(getline(ss,value,','))
            {
                meta_data.push_back(value);
                vector<float> newCol;
                data.push_back(newCol);
            }            
        }
        else 
        {
            int i = 0;
            while(getline(ss,value,','))
            {
                if(value.empty())
                {
                    i++;
                    continue;
                }
                data[i].push_back( stringToNum<float> (value));
                i++;
            }
        }
        count++;
	}
    
    for(int i = 0; i < meta_data.size(); i ++)
        ret.push_back( new DataUtil(meta_data[i], data[i]) );
    

    return ret;
};

/**
 * generate dummy data sets for flow, pressure and humidity under random seed 123.
 * flow sensor with 15 float numbers in range [-1000,-100] && [100, 1000]
 * pressure sensor with 20 integers in range [1000, 10000]
 * humidity sensor with 25 numbers with 1 decimal place in range [0,1]
 */ 
void GenerateARandomDataset()
{
    string name1 = "flow";
    vector<float> v1 = GenerateRandomFloats(123, 15, 100, 1000, true, true);
    DataUtil* flow = new DataUtil(name1, v1);

	string name2 = "pressure";
    vector<float> v2 = GenerateRandomFloats(123, 20, 1000, 10000, false, false);
    DataUtil* pressure = new DataUtil(name2, v2);


    string name3 = "humidity";
    vector<float> v3 = GenerateRandomFloats(123, 25, 0, 1, false, false);
    DataUtil* humidity = new DataUtil(name3, v3);

    WriteFile* wf = new WriteFile(flow->data, pressure->data, humidity->data);
    wf->WriteCSVToDir();
};


/**
 * mandatory parameter: the second argument in the command line interface should be the input data dir
 * optional parameter: the third argument is output file name.
 *                     the forth argument is output file dir without this argument will generate file in the source dir.
 * end with user press enter
 */
int main(int argc, char* argv[])
{
    GenerateARandomDataset();

    vector<DataUtil* > datasets = ReadFile(argv[1]);
    
    for(DataUtil* element: datasets)
        element->ToString();
    
    WriteFile* wf = new WriteFile(datasets[0]->data, datasets[1]->data, datasets[2]->data);
    if(argc == 3)
        wf->WriteCSVToDir("./", argv[2]);
    else if(argc == 4)
        wf->WriteCSVToDir(argv[3], argv[2]);
    
    getchar();
    return 1;
}