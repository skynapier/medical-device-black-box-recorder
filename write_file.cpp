#include <iostream> 
#include <fstream> 
#include <vector>
#include <sstream> 
#include <algorithm>


using namespace std;

/**
 * Class for write 3 sensors' data in order of flow, pressure and humidity.
 * Able to write into .csv file
 */ 
class WriteFile
{

    public:
    vector<float> flow, pressure, humidity;

	WriteFile( vector<float> fl,  vector<float> pr,  vector<float> hu)
    {
        flow = fl;
        pressure = pr;
        humidity = hu;
    }

    /**
     * csv's format is data split by ','
     * Prerequest is all of the datasets not null
     * Able for case that 3 datasets with different size
     * defalult is write an dummy data test file
     */ 
	void WriteCSVToDir(string outFileDir = "./", string fileName = "test_data")
	{
		if(flow.empty() || pressure.empty() || humidity.empty() )
            return;
        
        ofstream outFile; 
        
        outFileDir += + "/" + fileName + ".csv";
		//std::cout << outFileDir << std::endl;

		outFile.open(outFileDir,ios::out);

		outFile<<"flow"<<','<<"pressure"<<","<<"humidity"<<endl;

        int max_size =  max( max(flow.size(), pressure.size()), humidity.size());

        for(int i = 0; i < max_size; i++)
        {
            if(i < flow.size() )
                outFile << flow[i] << ','; 
            else
                outFile << "" << ','; 
            if(i < pressure.size() )
                outFile << pressure[i] << ',';
            else
                outFile << "" << ','; 
            if(i < humidity.size() )
                outFile << humidity[i] << endl;    
            else
                outFile << "" << endl;
        }

	}

};

/**
 * For given vector size.
 * Generate random float number between lower bound and upper bound 
 * with or without 4 decimals in random seed.
 * with or without negative number.
 * 
 * if want float number corresponding to 0% to 100% the lower_bound = 0, upper_bound =1 and false.
 */
vector<float> GenerateRandomFloats(int seed, int size, int lower_bound, int upper_bound, bool with_decimals, bool with_negative)
{
    srand(seed);
    vector<float> v1;

    for(int i = 0; i < size; i++){
        float test_data = rand() % (upper_bound - lower_bound)  + lower_bound;
        if( lower_bound == 0 && upper_bound ==1)
            test_data += float(rand() %10) / 10;
        
        if(with_decimals)
            test_data += float(rand() %1000) / 1000;
        
        if(with_negative && rand()%2 == 1)
            test_data = -test_data;
        
        //std::cout <<"generate number: " << test_data << std::endl;
        v1.push_back( test_data);
    }

    return v1;
}
