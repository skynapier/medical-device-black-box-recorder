# Preview
![](/preview.jpg)

# How to run my code
The library only needs a standard conformant C++11 compiler. It has no further dependencies. In this project directory, you can run 
### g++ -o recorder recorder.cpp
### ./recorder .~/test_data.csv

* mandatory parameter: the second argument in the command line interface should be the input data dir
* optional parameter: the third argument is output file name.
* optional parameter: the forth argument is output file dir without this argument will generate file in the source dir.
* end with user press enter

# UML class diagram
![Diagram](/UML-Class-Diagram.jpg)

# Feature
* able to read a csv file which contain different types and size of data within diffrent columns.
* able to output a csv file into any file directory. 

# Report
[Report in Google Drive](https://docs.google.com/document/d/1WtmiHaGlyAcbQM5bWWNJXek_b7Csd2h4eEdTqO6KDWs/edit?usp=sharing)