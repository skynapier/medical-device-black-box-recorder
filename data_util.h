#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

class DataUtil
{
    public:
        string name;
        vector <float> data;

        DataUtil(string n, vector<float> d) 
        {
            name = n;
            data = d;
        };

        /**
         * Show the data elements in the terminal in format
         * data: ..
         * size: ..
         * [a,b,c, ...]
         */ 
        void ToString()
        {
            if(!name.empty())
                cout << "data: " << name << endl;
            if(!data.empty())
            {
                cout << "size: " << data.size() << endl;
                cout << "[ ";
                for(auto element: data)
                {
                    cout << element << ',';
                }
                cout << " ]"<< endl;
            }
        };

};  

